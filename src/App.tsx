import "./App.css";
import { MainLayoutPage } from "./Pages/MainLayoutPage/MainLayoutPage";

function App() {
  return (
    <div>
      <MainLayoutPage />
    </div>
  );
}

export default App;
