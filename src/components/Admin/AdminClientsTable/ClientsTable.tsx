import React, { useState } from "react";
import styles from "./ClientsTable.module.scss";
export const ClientsTable = () => {
  const [newCLient, setNewClient] = useState(false);
  const [editClient, setEditClient] = useState(false);
  const [deleteClient, setDeleteClient] = useState(false);

  const addNewClient = () => {
    setNewClient(!newCLient);
  };

  const editClientDetails = () => {
    setEditClient(!editClient);
  };

  const deleteClientDetails = () => {
    setDeleteClient(!deleteClient);
  };
  return (
    <>
      <h1>Clients</h1>
      <button className={styles.addClient} onClick={addNewClient}>
        Add Client
      </button>
      <table className={styles.clientsTable}>
        <tr>
          <th>Client Name</th>
          <th>Email ID</th>
          <th>Contact</th>
          <th>Actions</th>
        </tr>
        <tr>
          <td>1</td>
          <td>ASM@gmail.com</td>
          <td>1235678</td>
          <td>
            {" "}
            <button className={styles.editBtn} onClick={editClientDetails}>
              Edit
            </button>
            <button className={styles.deleteBtn} onClick={deleteClientDetails}>
              Delete
            </button>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>SBM@gmail.com</td>
          <td>2356635</td>
          <td>
            <button className={styles.editBtn} onClick={editClientDetails}>
              Edit
            </button>
            <button className={styles.deleteBtn} onClick={deleteClientDetails}>
              Delete
            </button>
          </td>
        </tr>
      </table>

      {newCLient && (
        <div className={styles.addNewClientModel}>
          <div className={styles.addNewClientPopup}>
            <h2>Add New Client</h2>
            <input type="text" placeholder="Enter Client Name" />
            <input type="text" placeholder="Enter Email" />
            <input type="text" placeholder="Enter Contact Number" />
            <button className={styles.submitNewClient}>Submit</button>
            <button
              onClick={addNewClient}
              className={styles.closeNewCLientPopup}
            >
              Close
            </button>
          </div>
        </div>
      )}

      {editClient && (
        <div className={styles.editClientsModel}>
          <div className={styles.editClientsPopup}>
            <h2>Add New Client</h2>
            <input type="text" placeholder="Edit Client Name" />
            <input type="text" placeholder="Edit Email" />
            <input type="text" placeholder="Edit Contact Number" />
            <button className={styles.submitEditedClient}>Submit</button>
            <button
              onClick={editClientDetails}
              className={styles.closeClientsEditPopup}
            >
              Close
            </button>
          </div>
        </div>
      )}

      {deleteClient && (
        <div className={styles.deleteClientsModel}>
          <div className={styles.deleteClientsPopup}>
            <h2>Are you sure you want to delete?</h2>
            <div className={styles.deleteClientsButton}>
              <button className={styles.submitDeleteClientsPopup}>Yes</button>
              <button
                className={styles.closeDeleteCLientsPopup}
                onClick={deleteClientDetails}
              >
                No
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
