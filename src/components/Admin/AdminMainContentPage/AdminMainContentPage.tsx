import React, { useState } from 'react'
import { routes } from '../AdminLayout/AdminLayout.types';
import { AdminSidebar } from '../AdminSidebar/AdminSidebar'
import styles from "./AdminMainContentPage.module.scss"
export const AdminMainContentPage = () => {
  const [page, setPage] = useState("PO/PI");

  const onNavigate = (page: string) => {
      setPage(page);

      window.history.pushState({}, "", `/${page}`);
  }

  const Page = routes[page];
  return (
    <div className={styles.adminMainContentPage}>
        <AdminSidebar onNavigate={onNavigate}/>
      
        <main className={styles.adminDisplayContentPage}>
                 <Page/>
        </main>
    </div>
  )
}
