import React from 'react'
import { AdminButtons } from '../AdminButtons/AdminButtons'
import styles from "./AdminSidebar.module.scss"
export const AdminSidebar = ({onNavigate}:any) => {
  return (
    <div className={styles.adminSidebar}>
        <AdminButtons text='PO/PI' adminButtonChanges={() => onNavigate("PO/PI")}/>
        <AdminButtons text='Clients' adminButtonChanges={() => onNavigate("Clients")}/>
        <AdminButtons text='Furnance' adminButtonChanges={() => onNavigate("Furnance")}/>
        <AdminButtons text='HSN' adminButtonChanges={() => onNavigate("HSN")}/>
        <AdminButtons text='Materials' adminButtonChanges={() => onNavigate("Materials")}/>
        <AdminButtons text='Reports' adminButtonChanges={() => onNavigate("Reports")}/>
        <AdminButtons text='Storage' adminButtonChanges={() => onNavigate("Storage")}/>
        <AdminButtons text='Unit' adminButtonChanges={() => onNavigate("Unit")}/>
        <AdminButtons text='Users' adminButtonChanges={() => onNavigate("Users")}/>
    </div>
  )
}
