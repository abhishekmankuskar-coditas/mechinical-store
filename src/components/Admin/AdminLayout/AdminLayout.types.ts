import { AdminClientsPage } from "../../../Pages/AdminPage/AdminClientsPage/AdminClientsPage";
import { AdminFurnancePage } from "../../../Pages/AdminPage/AdminFurnancePage/AdminFurnancePage";
import { AdminHomePage } from "../../../Pages/AdminPage/AdminHomePage/AdminHomePage";
import { AdminHSNPage } from "../../../Pages/AdminPage/AdminHSNPage/AdminHSNPage";
import { AdminMaterialsPage } from "../../../Pages/AdminPage/AdminMaterialsPage/AdminMaterialsPage";
import { AdminReportPage } from "../../../Pages/AdminPage/AdminReportPage/AdminReportPage";
import { AdminStoragePage } from "../../../Pages/AdminPage/AdminStoragePage/AdminStoragePage";
import { AdminUnitPage } from "../../../Pages/AdminPage/AdminUnitPage/AdminUnitPage";
import { AdminUsersPage } from "../../../Pages/AdminPage/AdminUsersPage/AdminUsersPage";

export const routes: { [key: string]: () => JSX.Element } = {
  "PO/PI": AdminHomePage,
  "Clients": AdminClientsPage,
  "Furnance": AdminFurnancePage,
  "HSN": AdminHSNPage,
  "Materials": AdminMaterialsPage,
  "Reports": AdminReportPage,
  "Storage": AdminStoragePage,
  "Unit": AdminUnitPage,
  "Users": AdminUsersPage,
};
