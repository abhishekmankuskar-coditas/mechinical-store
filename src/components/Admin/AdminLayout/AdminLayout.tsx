import React, { useState } from 'react'
import { AdminHeader } from '../AdminHeader/AdminHeader'
import { AdminMainContentPage } from '../AdminMainContentPage/AdminMainContentPage'

export const AdminLayout = () => {


  return (
    <div>
        <AdminHeader/>
        <AdminMainContentPage/>
    </div>
  )
}
