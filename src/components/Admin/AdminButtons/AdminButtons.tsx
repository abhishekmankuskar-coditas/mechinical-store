import React from 'react'
import { IButtonProps } from './AdminButtons.types'
import styles from "./AdminButtons.module.scss"
export const AdminButtons = ({ adminButtonChanges, text }: IButtonProps) => {
    return (
        <button onClick={adminButtonChanges} className={styles.adminButtons}>{text}</button>
    )
}