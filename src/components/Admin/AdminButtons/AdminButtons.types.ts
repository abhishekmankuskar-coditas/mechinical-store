export interface IButtonProps {
    text : string,
    adminButtonChanges: () => void,
}