import React, { useState } from "react";
import styles from "./AccountantHeader.module.scss";
export const AccountantHeader = () => {
  const [accountantprofile, setAccountantprofile] = useState(false);
  const [resetPassword, setResetPassword] = useState(false);

  const openAccountantProfile = () => {
    setAccountantprofile(!accountantprofile);
  };

  const openResetPasswordModule = () => {
    if (accountantprofile) setResetPassword(!resetPassword);
    else {
      setAccountantprofile(accountantprofile);
    }
  };
  return (
    <>
      <div className={styles.accountantHeader}>
        <h2>Accountant DashBoard</h2>
        <button
          className={styles.accountantProfile}
          onClick={openAccountantProfile}
        >
          Profile
        </button>
        <button className={styles.accountantLogout}>Logout</button>
      </div>
      {accountantprofile && (
        <div className={styles.profileModel}>
          <div className={styles.profilePopup}>
            <h3>My Profile</h3>
            <h4>Name : Abhishek Mankuskar</h4>
            <h4>Email : mankuskarabhi2@@gmail.com</h4>
            <h4>Address : Pune</h4>
            <h4>Contact : 7387024822</h4>
            <button
              className={styles.resetPasswordBtn}
              onClick={openResetPasswordModule}
            >
              Reset Password
            </button>
            <button
              onClick={openAccountantProfile}
              className={styles.closeAccountantProfile}
            >
              Close
            </button>
          </div>
        </div>
      )}

      {resetPassword && (
        <div className={styles.resetPaswordModule}>
          <div className={styles.resetPasswordPopup}>
            <input type="password" placeholder="Enter your old password" />
            <input type="password" placeholder="Enter your new password" />
            <input type="password" placeholder="Renter your new password" />
            <button className={styles.checkResetedPassword}>check</button>
            <button
              onClick={openResetPasswordModule}
              className={styles.closeResetPasswordModule}
            >
              Close
            </button>
          </div>
        </div>
      )}
    </>
  );
};
