import React from 'react'
import { IClerkButtonnProps } from './ClerkButtons.types'
import styles from "./ClerkButtons.module.scss"
export const ClerkButtons = ({clerkButtonChanges,text} : IClerkButtonnProps) => {
  return (
    <button onClick={clerkButtonChanges} className={styles.clerkButtons}>{text}</button>
  )
}
