import { ClerkAddPOPage } from "../../../Pages/ClerkPage/ClerkAddPOPage/ClerkAddPOPage";
import { ClerkHomePage } from "../../../Pages/ClerkPage/ClerkHomePage/ClerkHomePage";

export const routes: { [key: string]: () => JSX.Element } = {
    "Home": ClerkHomePage,
    "Add Po": ClerkAddPOPage,
}
