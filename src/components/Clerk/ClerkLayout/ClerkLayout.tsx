import React, { useState } from 'react'
import { ClerkHeader } from '../ClerkHeader/ClerkHeader';
import { routes } from './ClerkLayout.data';
import styles from "./ClerkLayout.module.scss"
export const ClerkLayout = () => {

  const [page, setPage] = useState("Home");

  const onNavigate = (page: string) => {
      setPage(page);

      window.history.pushState({}, "", `/${page}`);
  }
  const Page = routes[page];
  return (
    <div className={styles.clerkMainPage}>
        <ClerkHeader onNavigate={onNavigate}/>
        <main>
          <Page/>
        </main>
    </div>
  )
}
