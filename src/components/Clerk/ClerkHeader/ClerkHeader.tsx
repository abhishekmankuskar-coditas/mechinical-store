import React, { useState } from "react";
import { ClerkButtons } from "../ClerkButtons/ClerkButtons";
import styles from "./ClerkHeader.module.scss";
export const ClerkHeader = ({ onNavigate }: any) => {
  const [clerkProfile, setClerkProfile] = useState(false);
  const [clerkResetPassword, setClerkResetPassword] = useState(false);

  const openClerkProfile = () => {
    setClerkProfile(!clerkProfile);
  };

  const openclerkProfileResetPasswordModel = () => {
    setClerkResetPassword(!clerkResetPassword);
  };

  return (
    <>
      <header className={styles.clerkHeader}>
        <h1>Clerk DashBoard</h1>
        <div className={styles.clerkBtns}>
          <ClerkButtons
            text="Home"
            clerkButtonChanges={() => onNavigate("Home")}
          />
          <ClerkButtons
            text="Add PO"
            clerkButtonChanges={() => onNavigate("Add Po")}
          />
          <button onClick={openClerkProfile}>Profile</button>
          <button>Logout</button>
        </div>
      </header>

      {clerkProfile && (
        <div className={styles.clerkprofileModel}>
          <div className={styles.clerkprofilePopup}>
            <h1>Profile</h1>
            <h4>Name : Abhishek Mankuskar</h4>
            <h4>Email : mankuskarabhi2@gmail.com</h4>
            <h4>Address : Latur</h4>
            <h4>Contact : 7383024822</h4>
            <button
              className={styles.resetClerkPasswordBtn}
              onClick={openclerkProfileResetPasswordModel}
            >
              Reset Password
            </button>
            <button
              onClick={openClerkProfile}
              className={styles.closeClerkProfile}
            >
              Close
            </button>
          </div>
        </div>
      )}

      {clerkResetPassword && (
        <div className={styles.clerkResetPaswordModule}>
          <div className={styles.clerkResetPasswordPopup}>
            <input type="password" placeholder="Enter your old password" />
            <input type="password" placeholder="Enter your new password" />
            <input type="password" placeholder="Renter your new password" />
            <button className={styles.checkClerkResetedPassword}>Check</button>
            <button
              onClick={openclerkProfileResetPasswordModel}
              className={styles.closeClerkResetPasswordModel}
            >
              Close
            </button>
          </div>
        </div>
      )}
    </>
  );
};
