import http from "./httpMethod";

export const postContent = async (credentials: {
  username: string;
  password: string;
}) => {
  const response = await http.post("auth/login", credentials);
  return response;
};

export const postForResetPassword = async (forgotPasswordCrediantals: {
  email: string;
}) => {
  const response = await http.post(
    "auth/forget-password",
    forgotPasswordCrediantals
  );
  return response;
};
