import { useState } from "react";
import {
  postContent,
  postForResetPassword,
} from "../../Services/http.services";
import styles from "../LoginPage/LoginPage.module.scss";

export const LoginPage = ({
  updatePage,
}: {
  updatePage: (currentPage: string) => void;
}) => {
  const [password, setPassword] = useState(false);
  const forgotPassword = () => {
    setPassword(!password);
  };
  const loginHandler = (event: any) => {
    event.preventDefault();
    const { username, password } = document.forms[0];
    const credentials = {
      username: username.value,
      password: password.value,
    };
    postLoginDetails(credentials);
  };

  const forgotPasswordHandler = (event: any) => {
    event.preventDefault();
    const { email } = document.forms[0];
    const forgotcredentials = {
      email: email,
    };
    postForgotPasswordDetails(forgotcredentials);
  };
  const postForgotPasswordDetails = async (forgotcredentials: any) => {
    const postForgotData = await postForResetPassword(forgotcredentials);
    console.log(postForgotData);
  };

  const postLoginDetails = async (crediantials: any) => {
    const postLoginData = await postContent(crediantials);

    if (postLoginData.data) {
      console.log(postLoginData.data);
      if (
        postLoginData.data.roleId === "0ed0971f-73e0-4221-b6c0-c50c96dcb07e"
      ) {
        updatePage("AccountantPage");
      } else if (
        postLoginData.data.roleId === "c298e02b-8bf5-4f55-b268-a160fcd2de84"
      ) {
        updatePage("AdminPage");
      } else if (
        postLoginData.data.roleId === "504d4500-5423-4aa9-bc4b-79e4f2871829"
      ) {
        updatePage("ClerkPage");
      }
    }
  };

  return (
    <div className={styles.loginPage}>
      <form className={styles.loginPageForm} onSubmit={loginHandler}>
        <h2>Login Here !!</h2>
        <input
          type="text"
          placeholder="Enter your username"
          className={styles.loginPageUsername}
          name="username"
        />
        <input
          type="password"
          placeholder="Enter your password"
          className={styles.loginPagePassword}
          name="password"
        />
        <button className={styles.loginButton} onClick={loginHandler}>
          Login
        </button>
        <button
          onClick={forgotPassword}
          className={styles.forgotPasswordButton}
        >
          Forgot password?
        </button>
      </form>

      {password && (
        <div className={styles.openForgotPasswordPage}>
          <form
            className={styles.forgotPasswordForm}
            onSubmit={forgotPasswordHandler}
          >
            <h2>Forgot Password !!</h2>
            <input
              type="email"
              placeholder="Enter your registered email"
              required
              name="email"
            />
            <button
              onClick={forgotPasswordHandler}
              className={styles.submitNewPasswordBtn}
            >
              Submit
            </button>
            <button
              onClick={forgotPassword}
              className={styles.closeForgotPasswordModule}
            >
              Close
            </button>
          </form>
        </div>
      )}
    </div>
  );
};
