import React from 'react'
import { AccountantHeader } from '../../components/Accountant/AccountantHeader/AccountantHeader'
import { AccountantMainPage } from '../../components/Accountant/AccountantMainPage/AccountantMainPage'

export const AccountantPage = () => {
  return (
    <div>
        <AccountantHeader/>
        <AccountantMainPage/>
    </div>
  )
}
