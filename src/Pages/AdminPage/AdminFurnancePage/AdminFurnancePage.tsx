import React from 'react'
import { AdminFurnanceTable } from '../../../components/Admin/AdminFurnanceTable/AdminFurnanceTable'

export const AdminFurnancePage = () => {
  return (
    <div>
      <AdminFurnanceTable/>
    </div>
  )
}
