import React from 'react'
import { AdminUsersTable } from '../../../components/Admin/AdminUsersTable/AdminUsersTable'

export const AdminUsersPage = () => {
  return (
    <div>
      <AdminUsersTable/>
    </div>
  )
}
