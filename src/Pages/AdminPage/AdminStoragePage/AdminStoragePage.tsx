import React from 'react'
import { AdminStorageTable } from '../../../components/Admin/AdminStorageTable/AdminStorageTable'

export const AdminStoragePage = () => {
  return (
    <div>
      <AdminStorageTable/>
    </div>
  )
}
