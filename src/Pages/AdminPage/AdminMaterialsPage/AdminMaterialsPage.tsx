import React from 'react'
import { AdminMaterialTable } from '../../../components/Admin/AdminMaterialTable/AdminMaterialTable'

export const AdminMaterialsPage = () => {
  return (
    <div>
      <AdminMaterialTable/>
    </div>
  )
}
