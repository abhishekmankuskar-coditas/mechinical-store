import React from 'react'
import { AdminUnitsTable } from '../../../components/Admin/AdminUnitsTable/AdminUnitsTable'

export const AdminUnitPage = () => {
  return (
    <div>
      <AdminUnitsTable/>
    </div>
  )
}
