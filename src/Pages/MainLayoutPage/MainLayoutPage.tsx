import React, { useState } from "react";
import { routes } from "./MainLayoutPage.data";
export const MainLayoutPage = () => {
  const [currentPage, setCurrentPage] = useState("LoginPage");

  function updatePage(currentPage: string) {
    setCurrentPage(currentPage);
  }
  const Page = routes[currentPage];

  return (
    <div>
      {currentPage === "LoginPage" && <Page updatePage={updatePage} />}
      {currentPage !== "LoginPage" && <Page />}
      {/* <LoginPage /> */}
      {/* <AccountantPage /> */}
      {/* <AdminLayout /> */}
      {/* <ClerkLayout /> */}
    </div>
  );
};
