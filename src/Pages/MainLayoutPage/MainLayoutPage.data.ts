import { Routes } from "react-router-dom";
import { AdminLayout } from "../../components/Admin/AdminLayout/AdminLayout";
import { ClerkLayout } from "../../components/Clerk/ClerkLayout/ClerkLayout";
import { AccountantPage } from "../AccountantPage/AccountantPage";
import { LoginPage } from "../LoginPage/LoginPage";

export const routes: { [key: string]: ({}: any) => JSX.Element } = {
  LoginPage: LoginPage,
  AccountantPage: AccountantPage,
  ClerkPage: ClerkLayout,
  AdminPage: AdminLayout,
};

// export const routes =()=> {
//   return(
//     <div>
//     <BrowserRouter>
//         <Routes>
//           <Route path="/" element={<Login />}></Route>
//           <Route path="/admin" element={<Admin />}>
//           <Route path="/clerk" element={<ClerkDashboard />}></Route>
//           <Route path="/accountant" element={<AccountantDashboard />}></Route>
//         </Routes>
//       </BrowserRouter>
//     </div>
//   );
// };
