import React from 'react'
import { ClerkHomePageTable } from '../../../components/Clerk/ClerkHomePageTable/ClerkHomePageTable'

export const ClerkHomePage = () => {
  return (
    <div>
        <ClerkHomePageTable/>
    </div>
  )
}
